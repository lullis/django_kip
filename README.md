## Kip

Kip is a django app that takes the links crawled by
[Boris Spiders](https://bitbucket.org/lullis/django_boris) and save them on IPFS.

Kip is one of the django applications developed for the nofollow.io
project.
